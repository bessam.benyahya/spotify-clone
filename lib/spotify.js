import SpotifyWebApi from "spotify-web-api-node";

const scopes = [
  "user-read-recently-played",
  "user-read-email",
  "playlist-read-collaborative",
  "playlist-read-private",
  "streaming",
  "user-read-private",
  "user-library-read",
  "user-top-read",
  "user-read-playback-state",
  "user-modify-playback-state",
  "user-read-currently-playing",
  "user-follow-read",
  "app-remote-control",
  "user-read-playback-position",
  "user-top-read",
  "user-read-playback-position",
].join(",");

const params = {
  scopes,
};

const queryParamsString = new URLSearchParams(params).toString();

const LOGIN_URL = `https://accounts.spotify.com/authorize?${queryParamsString}`;

const spotifyApi = new SpotifyWebApi({
  clientId: process.env.NEXT_PUBLIC_CLIENT_ID,
  clientSecret: process.env.NEXT_PUBLIC_CLIENT_SECRET,
});

export default spotifyApi;
export { LOGIN_URL };
